package com.example.razan2.viewpagerfrag;

public class Stedant {
    String name;
    String majer;

    public Stedant(String name, String majer) {
        this.name = name;
        this.majer = majer;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMajer() {
        return majer;
    }

    public void setMajer(String majer) {
        this.majer = majer;
    }
}
