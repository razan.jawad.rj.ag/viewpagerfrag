package com.example.razan2.viewpagerfrag;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class StedantAdapter extends RecyclerView.Adapter<StedantAdapter.StedanrHolder> {

    private ArrayList<Stedant> stedantArrayList;
private DeleteListner deleteListner;
    public StedantAdapter(ArrayList<Stedant> stedantArrayList,DeleteListner deleteListner) {
        this.stedantArrayList = stedantArrayList;
        this.deleteListner=deleteListner;
    }

    @NonNull
    @Override
    public StedanrHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =  LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_item, viewGroup,false);
        return new StedanrHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull StedanrHolder stedanrHolder, int i) {
        stedanrHolder.tvNmae.setText(stedantArrayList.get(i).getName());
        stedanrHolder.tvMager.setText(stedantArrayList.get(i).getMajer());


    }

    @Override
    public int getItemCount() {
        if(stedantArrayList!=null){return stedantArrayList.size();}
        return 0;
    }

    class StedanrHolder extends RecyclerView.ViewHolder{
        TextView tvNmae,tvMager;
        Button delete;

        public StedanrHolder(@NonNull View itemView) {
            super(itemView);
            tvNmae=itemView.findViewById(R.id.tv_name);
            tvMager=itemView.findViewById(R.id.tv_major);
            delete=itemView.findViewById(R.id.delete);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    deleteListner.onDelete(getAdapterPosition());
               stedantArrayList.remove(getAdapterPosition());
              // notifyItemChanged(getAdapterPosition());*/
               notifyItemRemoved(getAdapterPosition());

            // StedanrHolder.this.notify();

                }
            });
        }
    }
}
