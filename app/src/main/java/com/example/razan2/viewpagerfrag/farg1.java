package com.example.razan2.viewpagerfrag;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;


/**
 * A simple {@link Fragment} subclass.
 */
public class farg1 extends Fragment {

    Button bn1,bn2;
    ImageView img;
    float angle=0;
    public farg1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View  rootView= inflater.inflate(R.layout.fragment_farg1, container, false);
        bn1=rootView.findViewById(R.id.bn1);
        bn2=rootView.findViewById(R.id.bn2);
        img =rootView.findViewById(R.id.imageView);



         bn1.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {

              rot(1);
          }
      });


        bn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                rot(-1);
            }
        });


        return rootView;
    }

public void rot(int deraction)
{
    float rotation =90;
    rotation*=deraction;

    angle+=rotation;
    if(angle==360)
    {angle=0;}

    img.setRotation(angle);

}
}
