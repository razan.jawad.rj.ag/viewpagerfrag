package com.example.razan2.viewpagerfrag;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class farg2 extends Fragment {

    TextView tv;
    EditText ed;
    public farg2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_farg2, container, false);
        tv=rootView.findViewById(R.id.textView);
       ed=rootView.findViewById(R.id.editText);

       //tv.setText(cabetalFirst(""));
        ed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
               // String re=cabetalFirst("razan jawad");

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tv.setText(cabetalFirst(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });
        return rootView;
    }
    public String cabetalFirst( String a)
    {
        if(a.equals("")){return a;}
        else{
        String nameCapitalized="";
        String name[]=  a.split(" ");

        for (int i = 0; i <name.length ; i++) {
            String s1 = name[i].substring(0,1).toUpperCase();
             nameCapitalized += s1 + name[i].substring(1)+" ";
        }
        return nameCapitalized;}}
}
